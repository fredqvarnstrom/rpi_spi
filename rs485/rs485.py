import serial
import RPi.GPIO as GPIO


class RS485:

    ser = None
    dir_pin = 25

    def __init__(self, port='/dev/ttyS0', baudrate=19200, dir_pin=25):
        self.ser = serial.Serial(port=port, baudrate=baudrate)
        self.dir_pin = dir_pin
        self.initialize_gpio()

    def initialize_gpio(self):
        """
        Initialize GPIO
        :return:
        """
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.dir_pin, GPIO.OUT)
        GPIO.out(self.dir_pin, False)
        return True


def crc16(val):
    """
    // MODBUS RTU Error Checksum calculation using Polynomial
    :param val:
    :return:
    """
    pass


if __name__ == '__main__':

    a = RS485()
    print a
