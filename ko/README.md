# Pi-SPi-8KO Raspberry Pi Relay Output Interface

*NOTE:* It is **highly** recommended to use `GPIO22` for `SPI_CS`!

Please set `J2` jumper as `GPIO22` when using other boards together.

Otherwise conflict may occur and other boards will not work.

And this python class is supposed that `GPIO22` is used for `SPI_CS` pin.
