"""
*** Python Driver class for Relay Output Interface ***

IMPORTANT NOTE:
    It is highly recommended to use GPIO22 as SPI_CS pin.

Copyright 2016

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS

"""

import os
import sys
import math

# Add parent directory to path for `pispi.py`.
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
from pispi import PiSPI

__version__ = 0.1


class Relay(PiSPI):

    status = [False] * 8

    def __init__(self, cs_pin=22):                              # Relay Module uses GPIO22 for SPI_CS
        # PiSPI.__init__(self, cs_pin=cs_pin, speed=10000, bus=bus, device=device)   # Relay Module's clock is 100kHz
        PiSPI.__init__(self, cs_pin=cs_pin, speed=10000)     # Relay Module's clock is 100kHz
        self.close_all()
        self.status = [False] * 8

    def close_all(self):
        self.send_cmd([0xFF])
        self.status = [True] * 8

    def open_all(self):
        self.send_cmd([0x00])
        self.status = [False] * 8

    def set_relay(self, channel, value):
        """
        Set a given relay status
        :param channel: Channel number
        :param value: Open: True, Closed: False
        :return:
        """
        if not 0 <= channel < 8:
            raise ValueError('Error, channel number must be in 0 ~ 7')

        if self.status[channel] == value:
            print 'Already set'
            return True
        else:
            if value:
                print 'Closing channel {}'.format(channel)
            else:
                print 'Opening channel {}'.format(channel)

        self.status[channel] = value

        cmd = 0
        for i in range(len(self.status)):
            if self.status[i]:
                cmd += math.pow(2, (7-i))
        self.send_cmd([int(cmd)])
        return True

    def relay_open(self, channel):
        return self.set_relay(channel=channel, value=False)

    def relay_close(self, channel):
        return self.set_relay(channel=channel, value=True)


if __name__ == '__main__':

    a = Relay()

    # Open all relays
    a.open_all()

    # Close all relays
    a.close_all()

    # Open the channel 5
    a.relay_open(5)

    # Close the channel 5
    a.relay_close(5)
