/*
 * pispi_8di.c
 *
 * Copyright 2016
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS
	FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

// Define Pins as used by Pi-SPi modules
#define CS_2AO		4
#define CS_8DI		17
#define CS_SPARE2	27
#define CS_SPARE1	22
#define CS_LEDS		18
#define CS_LCD1		23
#define CS_LCD2		24
#define CS_8KO		8
#define CS_8AI		7
#define DIR_RS485	25

// Prototypes
void Initialize_Pi_Hardware(void);
unsigned char Update_8DI(void);
unsigned char Status_8DI;

// main
int main(void) {

	wiringPiSetupGpio();
  	Initialize_Pi_Hardware();
 	digitalWrite(CS_8DI, HIGH);

 	Status_8DI = 0;

	while(1)
	{
		Status_8DI = Update_8DI();
		printf("8DI Status = %02x \n", Status_8DI);
		delay(1000);
	}

	return 0;
}

unsigned char Update_8DI(void) {

	unsigned char data;
	unsigned char buf[5];

	wiringPiSPISetup(1, 100000);

	buf[0] = 0x40;	// Write command
	buf[1] = 0x00;	// Start Address 00 IODIR
	buf[2] = 0xff;	// Set IODIR to Inputs
	buf[3] = 0xff;	// Set IPOL Polarity to Invert

	digitalWrite(CS_8DI, LOW);
	wiringPiSPIDataRW(1,buf,4);
	digitalWrite(CS_8DI, HIGH);

	delay(5);

	buf[0] = 0x41;	// Read Command
	buf[1] = 0x09;	// Address 0
	buf[2] = 0x00;	// Dummy Byte

	digitalWrite(CS_8DI, LOW);
	wiringPiSPIDataRW(1,buf,3);
	data = buf[2] & 0xff;
	digitalWrite(CS_8DI, HIGH);

	return data;
}

void Initialize_Pi_Hardware(void) {

	// SPI CS Enable Lines
	pinMode(CS_2AO, OUTPUT);
	pinMode(CS_8DI, OUTPUT);
	pinMode(CS_SPARE2, OUTPUT);
	pinMode(CS_SPARE1, OUTPUT);
	pinMode(CS_LEDS, OUTPUT);
	pinMode(CS_LCD1, OUTPUT);
	pinMode(CS_LCD2, OUTPUT);
	pinMode(CS_8KO, OUTPUT);
	pinMode(CS_8AI, OUTPUT);
	pinMode(DIR_RS485, OUTPUT);
}


